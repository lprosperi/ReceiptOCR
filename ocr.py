from crop import process_image
from rotation import correct_skew

from tesserocr import PyTessBaseAPI, PSM
import argparse
from PIL import Image

def add_border(img, size=15):
    width, height = img.size
    n_img = Image.new('L', (width+2*size,height+2*size), color=255)
    n_img.paste(img,(size,size))
    return n_img

from skimage.filters import threshold_yen, threshold_minimum, threshold_local
from skimage import exposure
from numpy import asarray
import matplotlib
import matplotlib.pyplot as plt

from skimage.filters import try_all_threshold

def pre_binarization(img, threshold=110):
    x,y = img.size
    image = asarray(img)
    for i in range(x):
        for j in range(y):
            img.putpixel( (i,j), 255 if image[j][i] > threshold else 0 )

    return img         

def pre(img, threshold=110):
    x,y = img.size
    image = asarray(img)
    for i in range(x):
        for j in range(y):
            img.putpixel( (i,j), image[j][i] if image[j][i] > threshold else 0 )

    return img  
def binarize(img, threshold=30):
    image = asarray(img)
   # image = exposure.rescale_intensity(image)
    #Image.fromarray(image).show()
    #fig, ax = try_all_threshold(image, figsize=(10, 8), verbose=False)
    #plt.show()
    _global = threshold_minimum(image)
    binary = image > _global
    x,y =img.size
    
    for i in range(x):
        for j in range(y):
            img.putpixel( (i,j), 255 if binary[j][i] else 0)

    return img
     
def resize(img,MAX=600):
    width, height = img.size
    
    if min(width,height) <= MAX:
        return add_border(img) 
    elif width > height:
        img.thumbnail((width*(MAX/height),MAX))            
    else:       
        img.thumbnail((MAX,height*(MAX/width)))            
    
    return add_border(img)

parser = argparse.ArgumentParser()
parser.add_argument('filename', type=str, help='Location of the file to process')
parser.add_argument('--out', default='out.jpg', help='The basename of the output file') 
parser.add_argument('--skew',  action="store_true", help='Try to put a page straight, caution : this can be very time consuming (disallowed by default)')
parser.add_argument('--smooth',  action="store_true", help='Try to put a page straight, caution : this can be very time consuming (disallowed by default)')
parser.add_argument('--binarize', action="store_true")
args = parser.parse_args()

from PIL import ImageFilter, ImageOps

img = Image.open(args.filename)

img=img.convert('L')
if args.smooth :
    img = img.filter(ImageFilter.SMOOTH)

#Extract text
img = process_image(img)

if args.binarize:
    #Basic remove background
    hist = img.histogram()
    a = sum(hist)/len(hist)
    img = img.point(lambda x: x if x < a else -1) 

    img = binarize(img)
else:
    img = pre_binarization(img)

if  args.skew :
    img = correct_skew( img )

img = img.filter(ImageFilter.SMOOTH_MORE)
img.save(args.out)
#with PyTessBaseAPI(psm=PSM.AUTO_OSD) as api:
#    help(api)
#    api.SetImage(img)
#    print(api.GetUTF8Text())
