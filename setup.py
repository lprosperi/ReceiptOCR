from distutils.core import setup
import os

setup(name="ReceiptOCR",
    version="0.0.0.dev1",
    description="A basic receipt OCR",
    author="Laurent Prosperi",
    author_email="laurent.prosperi@ens-cachan.fr",
    url="",
    platforms=["linux"],
    license="Apache 2.0",
    download_url="",
    classifiers=[
        'Development Status :: 2 - Pre-Alpha', 
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: Apache Software License',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6'
    ],
    packages=["ReceiptOCR"],
    scripts = ['ocr'],
    keywords= ["ocr", "receipt"],
    package_dir = {"Receipt": "src"},
    requires=["PIL", "numpy", "scikit-image", "scipy", "tesserocr"],
    package_data={}
)
